package tech.codingzen.kdi_jvm

import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import tech.codingzen.kdi.logging.Logger.Level

class Log4j2Logger(
  private val logger: Logger,
  level: Level = Level.INFO
) : tech.codingzen.kdi.logging.Logger {
  constructor(name: String, level: Level = Level.INFO) : this(LogManager.getLogger(name), level)

  override fun log(level: Level, block: () -> String) = when (level) {
    Level.OFF -> Unit
    Level.ERROR -> logger.error(block)
    Level.WARN -> logger.warn(block)
    Level.TRACE -> logger.trace(block)
    Level.INFO -> logger.info(block)
  }

  override fun log(level: Level, throwable: Throwable, block: () -> String) = when (level) {
    Level.OFF -> Unit
    Level.ERROR -> logger.error(block, throwable)
    Level.WARN -> logger.warn(block, throwable)
    Level.TRACE -> logger.trace(block, throwable)
    Level.INFO -> logger.info(block, throwable)
  }
}