package tech.codingzen.kdi_jvm

class MainArgs(val args: Array<String>) {
  operator fun get(index: Int) = args[index]
}