package tech.codingzen.kdi_jvm

interface SystemEnv {
  operator fun get(key: String): String?

  object Impl : SystemEnv {
    override fun get(key: String): String? = System.getenv(key)
  }
}