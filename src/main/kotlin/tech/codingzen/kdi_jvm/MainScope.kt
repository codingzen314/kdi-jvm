package tech.codingzen.kdi_jvm

import tech.codingzen.kdi.data_structure.Kdi
import tech.codingzen.kdi.spec.BaseSpec

class MainScope(private val args: Array<String>) : BaseSpec.Delegate {
  companion object {
    const val scopeId = "main"
  }

  override val scopeSpec: BaseSpec = Kdi.scopeSpec(scopeId)
    .noBootstrapper()
    .module {
      instance(MainArgs(args))
      instance<SystemEnv>(SystemEnv.Impl)
    }
    .build()
}